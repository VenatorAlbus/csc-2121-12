package brass;

import java.util.List;

class BrassSellCoalAndIronComputerPlayer implements BrassComputerPlayer
{
	private BrassGame brass_game;
	private int TurnCounter;
	private boolean HasSoldCotton;
	private boolean BuiltACotton;
	private boolean LoanHasBeenTaken;
	private boolean CoalUpgraded;
	private int ShipyardUpgrade;
	private int LinkCounter;
	
	public BrassSellCoalAndIronComputerPlayer(BrassGame bg)
	{
		brass_game = bg;
		BuiltACotton = false;
		LoanHasBeenTaken = false;
		HasSoldCotton = false;
		CoalUpgraded = false;
		ShipyardUpgrade = 0;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		int computer_player_id = brass_game.getActivePlayerID();

		// Avoiding magic numbers! (kind of)
		int CoalIndustryValue = 20;
		int CottonIndustryValue = 21;
		int IronIndustryValue = 22;
		int PortIndustryValue = 23;
		int ShipyardIndustryValue = 24;

		int LoanThreshold = 12;

		// This means first turn out of the gate!
		// Yes, this is us hard coding our first turn. We were too lazy to make it super sophisticated.
		// It could be done though.
		if (brass_game.isFirstTurn())
		{
			// Check to see if we have a cotton mill card!
			int CottonCardToUse = GetIndustryOrCityCard(computer_player_id, CottonIndustryValue);
			if (CottonCardToUse > 0)
			{
				// Do we have a cotton card we can build in an occupid port?
				if(CheckPortCitiesWithCottonCard(computer_move, CottonCardToUse))
				{
					BuiltACotton = true;
					return computer_move;
				}

				// Check if the port cities are occupid and we have the card to join them.
				else if (CheckPortCitiesWithCitiesCards(computer_move))
				{
					BuiltACotton = true;
					return computer_move;
				}

				// Do we have a cotton card we can build in an external location?
				else if (CheckExternalCitiesWithCottonCard(computer_move, CottonCardToUse))
				{
					BuiltACotton = true;
					return computer_move;
				}
			}

			// Do we have a port city card someone else is already at? (But not full of course)
			else if (CheckPortCitiesWithCitiesCards(computer_move))
			{
				BuiltACotton = true;
				return computer_move;
			}

			// Do we have the external cities cards to build an external city?
			else if(CheckExternalCitiesWithCitiesCards(computer_move))
			{
				BuiltACotton = true;
				return computer_move;
			}

			// Nowhere is especially great to build, so lets Dr. Boshart code take care of it!
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, BrassIndustryEnum.COTTON.getValue());
			if (computer_move.isActionSelected()) 
			{
				BuiltACotton = true;
				return computer_move;
			}

		} // End of turn 1 check
		
		// So we have a mill, but haven't sold our cotton yet.
		if(BuiltACotton && !HasSoldCotton)
		{
			boolean can_sell_cotton = brass_game.canSellCotton(computer_player_id);
			can_sell_cotton = brass_game.canSellCotton(computer_player_id);
			// Someone else has completed our route, or we buddyed up with someone's port. 
			if(can_sell_cotton && !LoanHasBeenTaken)
			{
				LoanHasBeenTaken = true;
				computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
				return computer_move;
			}

			// Loan has been taken, and we can sell our lovely cotton.
			else if(can_sell_cotton && LoanHasBeenTaken)
			{
				HasSoldCotton = true;
				computer_move.selectSellCottonAction(getCardForNonBuildAction(computer_player_id));
				return computer_move;
			}

			// We need to find a link to establish to help sell our cotton!
			else 
			{
				getAllInfoForLinkToSellCottonAction(computer_move, computer_player_id, 1);
				if (computer_move.isActionSelected()) 
				{
					return computer_move;
				}
			}
		}

		if (HasSoldCotton)
		{
			if (!CoalUpgraded)
			{
				CoalUpgraded = true;
				computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 1, 3);
				return computer_move;
			}

			// Check to see if we already have atleast one unflipped coal plant (so we still have coal), and there is links up for grabs
			if(brass_game.countAllPlayersUnflippedIndustry(CoalIndustryValue - 19, computer_player_id) > 0)
			{
				if(brass_game.countAllPlayersUnflippedIndustry(IronIndustryValue - 19, computer_player_id) > 0)
				{
					if(ShipyardUpgrade < 3)
					{
						ShipyardUpgrade = ShipyardUpgrade + 2;
						computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 5, 5);
						return computer_move;
					}

					// We are in the second game phase
					if(ShipyardUpgrade == 3 && brass_game.getBrassPhase())
					{
						ShipyardUpgrade++;
						computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 5, 2);
						return computer_move;
					}
				}

				else if(brass_game.getBrassPhase() && LinkCounter < 6)
				{
					// Sort links by victory points (coins)
					getAllInfoForSortedLinkAction(computer_move, computer_player_id);
					if(computer_move.isActionSelected()) 
					{ 
						LinkCounter++;
						return computer_move;
					}
				}
			}

			// First try to build a shipyard
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, ShipyardIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				ShipyardUpgrade++;
				return computer_move;
			}

			// Then iron works
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, IronIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}

			// Then coal mines
			getAllInfoForIndustrySpecificBuildAction(computer_move, computer_player_id, CoalIndustryValue - 19);
			if(computer_move.isActionSelected())
			{
				return computer_move;
			}
		}

		// Sort links by victory points (coins)
		getAllInfoForSortedLinkAction(computer_move, computer_player_id);
		if(computer_move.isActionSelected()) 
		{ 
			return computer_move;
		}

		// Take a loan if we get too low
		int player_money = brass_game.getMoney(computer_player_id);
		if(player_money < LoanThreshold)
		{
			computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
			return computer_move;
		}

		// Nothing to buy or build, but still have some money, just upgrade for the heck of it.
		if(brass_game.isTokenStackEmpty(computer_player_id, CottonIndustryValue - 19))
		{
			computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 2, 2);
			return computer_move;
		}

		if(brass_game.isTokenStackEmpty(computer_player_id, PortIndustryValue - 19))
		{
			computer_move.selectTechUpgradeAction(getCardForNonBuildAction(computer_player_id), 4, 4);
			return computer_move;
		}

		
	System.out.println("Discard!");
		computer_move.selectDiscardAction(getCardForNonBuildAction(computer_player_id));
		return computer_move;
	}

	// ******************************* Functions I have added

	private int GetIndustryOrCityCard(int player_id, int industry_or_city_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		int brass_card_city_tech_id;

		for (int i = 1; i <= num_cards; i++)
		{
			brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id == industry_or_city_id)
			{
				return i;
			}
		}

		return -1;
	}

	private boolean CheckPortCitiesWithCottonCard(BrassComputerPlayerAction computer_move, int card_to_use)
	{
		// Port and Cotton Cities
		int LancasterCityID = 10;
		int PrestonCityID = 15;
		int WarringtonCityID = 18;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		if(brass_game.isCityConnectedToConstructedPort(PrestonCityID) && brass_game.canBuildIndustry(false, PrestonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, PrestonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.isCityConnectedToConstructedPort(LancasterCityID) && brass_game.canBuildIndustry(false, LancasterCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, LancasterCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.isCityConnectedToConstructedPort(WarringtonCityID) && brass_game.canBuildIndustry(false, WarringtonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, WarringtonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else
		{
			return false;
		}
	}

	private boolean CheckPortCitiesWithCitiesCards(BrassComputerPlayerAction computer_move)
	{
		// Port and Cotton Cities
		int LancasterCityID = 10;
		int PrestonCityID = 15;
		int WarringtonCityID = 18;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		int LancasterHandID = GetIndustryOrCityCard(computer_player_id, LancasterCityID);
		int PrestonHandID = GetIndustryOrCityCard(computer_player_id, PrestonCityID);
		int WarringtonHandID = GetIndustryOrCityCard(computer_player_id, WarringtonCityID);

		if(LancasterHandID >= 0 && brass_game.isCityConnectedToConstructedPort(PrestonCityID) && brass_game.canBuildIndustry(true, PrestonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(LancasterHandID, PrestonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(PrestonHandID >= 0 && brass_game.isCityConnectedToConstructedPort(LancasterCityID) && brass_game.canBuildIndustry(true, LancasterCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(PrestonHandID, LancasterCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else if(WarringtonHandID >= 0 && brass_game.isCityConnectedToConstructedPort(WarringtonCityID) && brass_game.canBuildIndustry(true, WarringtonCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(WarringtonHandID, WarringtonCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		else
		{
			return false;
		}
	}

	private boolean CheckExternalCitiesWithCottonCard(BrassComputerPlayerAction computer_move, int card_to_use)
	{
		// Cotton Cities Adjacent to External Market
		int ColneCityID = 7;
		int MacclesfieldCityID = 12;
		int RochdaleCityID = 16;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		// Check all the Cotton Cities Adjacent to External Market
		for (int i = 1; i < 5; i++) // Check all the other players tokens in these cities.
		{
			if(i == computer_player_id)
			{
				break;
			} 

			else if(brass_game.getNumTokensInCity(ColneCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, ColneCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, ColneCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(brass_game.getNumTokensInCity(MacclesfieldCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(brass_game.getNumTokensInCity(RochdaleCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(false, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(card_to_use, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}
		}

		// If all that failed it means no one has built a port in a CM/P city, nor has anyone built another cotton mill on CMAEM cities
		// This means we will just have to start the external build.
		if (brass_game.canBuildIndustry(false, ColneCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, ColneCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		if (brass_game.canBuildIndustry(false, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		if (brass_game.canBuildIndustry(false, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
		{
			computer_move.selectBuildAction(card_to_use, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
			return true;
		}

		return false;
	}

	private boolean CheckExternalCitiesWithCitiesCards(BrassComputerPlayerAction computer_move)
	{
		// Check all the Cotton Cities Adjacent to External Market
		int ColneCityID = 7;
		int MacclesfieldCityID = 12;
		int RochdaleCityID = 16;
		int CottonIndustryValue = 21;
		int computer_player_id = brass_game.getActivePlayerID();

		int ColneHandID = GetIndustryOrCityCard(computer_player_id, ColneCityID);
		int MacclesfieldHandID = GetIndustryOrCityCard(computer_player_id, MacclesfieldCityID);
		int RochdaleHandID = GetIndustryOrCityCard(computer_player_id, RochdaleCityID);

		for (int i = 1; i < 5; i++) // Check all the other players tokens in these cities.
		{
			if (ColneHandID < 0 && MacclesfieldHandID < 0  && RochdaleHandID < 0 ) // Not even worth checking if we don't have one
			{
				break;
			}

			if(i == computer_player_id)
			{
				break;
			} 

			else if(ColneHandID >= 0 && brass_game.getNumTokensInCity(ColneCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, ColneCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(ColneHandID, ColneCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(MacclesfieldHandID >= 0 && brass_game.getNumTokensInCity(MacclesfieldCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, MacclesfieldCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(MacclesfieldHandID, MacclesfieldCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}

			else if(RochdaleHandID >= 0 && brass_game.getNumTokensInCity(RochdaleCityID, i) != 0)
			{
				if (brass_game.canBuildIndustry(true, RochdaleCityID, CottonIndustryValue - 19, computer_player_id))
				{
					computer_move.selectBuildAction(RochdaleHandID, RochdaleCityID, CottonIndustryValue - 19, 0, 0);
					return true;
				}
			}
		}
		return false;
	}

	private boolean CheckShipyardBuilding(BrassComputerPlayerAction computer_move, int player_id)
	{
		int BarrowCityID = 1;
		int BirkenheadCityID = 2;
		int LiverpoolCityID = 11;
		int ShipyardIndustryValue = 24;
		int LiverpoolHandID = GetIndustryOrCityCard(player_id, LiverpoolCityID);
		int ShipyardCard = GetIndustryOrCityCard(player_id, ShipyardIndustryValue);

		if(brass_game.canBuildIndustry(false, LiverpoolCityID, ShipyardIndustryValue - 19, player_id))
		{
			computer_move.selectBuildAction(ShipyardCard, LiverpoolCityID, ShipyardIndustryValue - 19, 0, 0);
			return true;
		}

		else if(brass_game.canBuildIndustry(true, LiverpoolCityID, ShipyardIndustryValue - 19, player_id))
		{
			computer_move.selectBuildAction(LiverpoolHandID, LiverpoolCityID, ShipyardIndustryValue - 19, 0, 0);
			return true;
		}

		return false;
	}

	// ******************************* Functions that had already existed

	private int getCardForNonBuildAction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				
				//duplicate city cards or already built in city (and canal phase)
				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}

			if (brass_card_city_tech_id == 21) return i;  //Cotton Mills
			if (brass_card_city_tech_id == 23) return i;  //Ports
			if (brass_card_city_tech_id == 7)  return i;  //Colne
			if (brass_card_city_tech_id == 8) return i;   //Ellesmere
			if (brass_card_city_tech_id == 10) return i;  //Lancaster
			if (brass_card_city_tech_id == 12) return i;  //Macclesfield
			
			//duplicate industry cards
			if (brass_card_city_tech_id > 19)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate && industry_id != 20) return i; // Avoid disgarding coal;
				}
			}
		}
		
		//second pass through the cards
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 24) return i; //shipyard industry card
			if (brass_card_city_tech_id == 1)  return i; //barrow
			if (brass_card_city_tech_id == 2) return i;  //birkenhead
		}
		 
		 //pick a random card to use for the non build action
		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}
	//System.out.println("random_discard: " + random_discard);
		return random_discard;
	}
	
	private void getAllInfoForIndustrySpecificBuildAction(BrassComputerPlayerAction computer_move, int player_id, int industry_id)
	{
		//loop over computer player cards
		//find a card that corresponds to something the computer player can build
		//select that card and the build action
		int num_cards = brass_game.getNumCards(player_id);
		int city_id;
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
				
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			if (brass_card_city_tech_id <= 19)
			{
				city_id = brass_card_city_tech_id;
				if (brass_game.canBuildIndustry(true, city_id, industry_id, player_id))
				{
					int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
					int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
					if (coal_city_id >= 0 && iron_city_id >= 0) 
					{
						computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
						return;
					}
				}
			}
			else
			{
				int card_industry_id = brass_card_city_tech_id - 19;
				if (industry_id == card_industry_id)
				{
					//what cities can this industry be built in?
					for (int j = 1; j <= 19; j++)
					{
						city_id = j;
						if (brass_game.canBuildIndustry(false, city_id, industry_id, player_id))
						{
							int coal_city_id = brass_game.canMoveCoal(city_id, industry_id, player_id);
							int iron_city_id = brass_game.canMoveIron(industry_id, player_id);
							if (coal_city_id >= 0 && iron_city_id >= 0) 
							{
								computer_move.selectBuildAction(i, city_id, industry_id, coal_city_id, iron_city_id);
								return;
							}
						}
					}
				}
			}
		}
	}
	
	//handles an arbitrary number of connected links
	private void getAllInfoForLinkToSellCottonAction(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth_limit)
	{
		if (max_depth_limit < 1) max_depth_limit = 1;
		int num_connections = brass_game.getNumLinks();

		int curr_depth_limit = 1;
		int curr_depth = 1;
		
		while(!computer_move.isActionSelected() && curr_depth_limit <= max_depth_limit)
		{
			//if a good link combination is found, this top level loop
			//determines the link to build
			for (int i = 1; i <= num_connections; i++)
			{
				//as soon as a link is found, stop
				if (computer_move.isActionSelected()) return;
				
				boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
				if (can_build_link)
				{
					//temporarily build the link (by simply setting the player_id of the link to computer_player_id)
					brass_game.buildTestLink(i, computer_player_id);
					boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
					if (can_player_sell_cotton)
					{
						computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), i);
	System.out.println("Sell cotton link top level");					
					}
					else if (curr_depth < curr_depth_limit)
					{
						getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, curr_depth_limit, curr_depth + 1, num_connections, i);
					}
					//remove the temporary link (by simply setting the player_id of the link to 0)
					brass_game.removeTestLink(i);
				}
			}
			curr_depth_limit++;
		}
	}
	
	private void getAllInfoForLinkToSellCottonActionRec(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth, int curr_depth, int num_connections, int first_connection)
	{
		for (int i = 1; i <= num_connections; i++)
		{
			//as soon as a link is found, stop
			if (computer_move.isActionSelected()) return;
			
			boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
			if (can_build_link)
			{
				//temporarily build the link (by simply setting the player_id of the link to computer_player_id)
				brass_game.buildTestLink(i, computer_player_id);
				boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
				if (can_player_sell_cotton)
				{
					computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), first_connection);
	System.out.println("Sell cotton link depth = " + curr_depth);					
				}
				else if (curr_depth < max_depth)
				{
					getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, max_depth, curr_depth + 1, num_connections, first_connection);
				}
				//remove the temporary link (by simply setting the player_id of the link to 0)
				brass_game.removeTestLink(i);
			}
		}
	}
	
	private void getAllInfoForSortedLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{	
		List<Integer> sorted_links = brass_game.getSortedLinks();
		int num_connections = sorted_links.size();
		for (int i = 1; i <= num_connections; i++)
		{
			int link_id = sorted_links.get(i-1);
			boolean can_build_link = brass_game.canBuildLink(link_id, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), link_id);
System.out.println("Sorted links");				
				return;
			}
		}
	}
	
	private void getAllInfoForLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{
		int num_connections = brass_game.getNumLinks();
		for (int i = 1; i <= num_connections; i++)
		{
			boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), i);
System.out.println("Regular Link");					
				return;
			}
		}
	}
}
