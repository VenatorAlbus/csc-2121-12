package brass;

interface BrassComputerPlayerState
{
	public BrassComputerPlayerAction getBrassMove();
}
