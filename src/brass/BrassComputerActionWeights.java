package brass;

import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

import util.ReadTextFile;
import util.WriteTextFile;

public class BrassComputerActionWeights
{
    private HashMap<String,Integer> weights;
        
    public BrassComputerActionWeights()
	{
	    weights = new HashMap<String,Integer>();
	}

    public void readFile(String filename)
	{
	    ReadTextFile file = new ReadTextFile(filename);
	    while (!file.EOF())
	    {
		String line = file.readLine();

		if (line != null && line.contains(":"))
		{
		    String[] fields = line.split(":",0);

		    weights.put(fields[0],Integer.parseInt(fields[1]));
		}
	    }
	    
	}

    public void writeFile(String filename)
	{
	    Set<String> keys = weights.keySet();
	    Iterator<String> iter = keys.iterator();
	    WriteTextFile file = new WriteTextFile(filename);
	    while (iter.hasNext())
	    {
		String key = iter.next();
		file.writeLine(key + ":" + weights.get(key));
	    }
	    file.close();
	}

    public Integer get(String key)
	{
	    return weights.get(key);
	}

    public void set(String key, Integer weight)
	{
	    weights.put(key,weight);
	}
    
    public void add(String key, Integer weight)
	{
	    weights.put(key,weight);
	}
    
    public Set keySet()
	{
	    return weights.keySet();
	}

    public int size()
	{
	    return weights.size();
	}
}
