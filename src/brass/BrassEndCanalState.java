package brass;

import java.util.List;

class BrassEndCanalState implements BrassComputerPlayerState
{
	private BrassGame brass_game;
	private BrassAIComputerPlayer computer_player;
	private boolean coal_upgraded;
	private boolean iron_upgraded;
	
	public BrassEndCanalState(BrassGame bg, BrassAIComputerPlayer cp)
	{
		brass_game = bg;
		computer_player = cp;
		coal_upgraded = false;
		iron_upgraded = false;
	}
	
	public BrassComputerPlayerAction getBrassMove()
	{
		System.out.println("END CANAL STATE!");
		if(brass_game.getBrassPhase()){
			computer_player.changeState(computer_player.getBuyPortState());
			return computer_player.getBuyPortState().getBrassMove();
		}
		int computer_player_id = brass_game.getActivePlayerID();
		int player_money = brass_game.getMoney(computer_player_id);
		int player_income = brass_game.getIncome(computer_player_id);
		
		BrassComputerPlayerAction computer_move = new BrassComputerPlayerAction();
		
		int num_actions_already_taken = brass_game.getNumActionsTaken(computer_player_id);
		boolean can_sell_cotton = brass_game.canSellCotton(computer_player_id);
		
		if(!coal_upgraded && !iron_upgraded){
			coal_upgraded = true;
			iron_upgraded = true;
			int card_index = getCardForNonBuildAction(computer_player_id);
			computer_move.selectTechUpgradeAction(card_index, BrassIndustryEnum.COAL.getValue(), BrassIndustryEnum.IRON.getValue());
			return computer_move;
		}
		
		if(player_income >= 3){
			computer_move.selectTakeLoanAction(getCardForNonBuildAction(computer_player_id), 3);
			return computer_move;
		}
		
		//sort links by victory points (coins)
		getAllInfoForSortedLinkAction(computer_move, computer_player_id);
		if (computer_move.isActionSelected()) return computer_move;
		
	System.out.println("Discard!");
		computer_move.selectDiscardAction(getCardForNonBuildAction(computer_player_id));
		return computer_move;
	}

	private int getCardForNonBuildAction(int player_id)
	{
		int num_cards = brass_game.getNumCards(player_id);
		boolean brass_phase = brass_game.getBrassPhase();
		
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id < 20)
			{
				int city_id = brass_card_city_tech_id;
				if (brass_game.isCityFull(city_id)) return i;
				
				//duplicate city cards or already built in city (and canal phase)
				if (!brass_phase)
				{
					int num_tokens_in_city = brass_game.getNumTokensInCity(city_id, player_id);
					if (num_tokens_in_city > 0) return i;
					
					for (int j = i+1; j <= num_cards; j++)
					{
						int city_id_duplicate = brass_game.getCardCityTechID(j);
						if (city_id == city_id_duplicate) return i;
					}
				}
			}
		
			if (brass_card_city_tech_id == 24) return i; //shipyard industry card
			if (brass_card_city_tech_id == 1)  return i; //barrow
			if (brass_card_city_tech_id == 2) return i;  //birkenhead
			
			//duplicate industry cards
			if (brass_card_city_tech_id > 19)
			{
				int industry_id = brass_card_city_tech_id;// - 19;
				for (int j = i+1; j <= num_cards; j++)
				{
					int industry_id_duplicate = brass_game.getCardCityTechID(j);
					if (industry_id == industry_id_duplicate) return i;
				}
			}
		}
		
		//second pass through the cards
		for (int i = 1; i <= num_cards; i++)
		{
			if (!brass_game.canSelectCard(i, player_id)) continue;
			int brass_card_city_tech_id = brass_game.getCardCityTechID(i);
			
			if (brass_card_city_tech_id == 6) return i;  //bury
			if (brass_card_city_tech_id == 19) return i;  //wigan
			
			if (brass_card_city_tech_id == 22) return i;  //iron works
			if (brass_card_city_tech_id == 20) return i;  //coal
			
			if (brass_card_city_tech_id == 14) return i;  //oldham
			if (brass_card_city_tech_id == 5) return i;  //burnley
		}
		 
		 //pick a random card to use for the non build action
		util.Random rand = util.Random.getRandomNumberGenerator();
		int random_discard = rand.randomInt(1, num_cards);
		while (!brass_game.canSelectCard(random_discard, player_id))
		{
			random_discard = rand.randomInt(1, num_cards);
		}
	//System.out.println("random_discard: " + random_discard);
		return random_discard;
	}
	
	
	//handles an arbitrary number of connected links
	private void getAllInfoForLinkToSellCottonAction(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth_limit)
	{
		if (max_depth_limit < 1) max_depth_limit = 1;
		int num_connections = brass_game.getNumLinks();

		int curr_depth_limit = 1;
		int curr_depth = 1;
		
		while(!computer_move.isActionSelected() && curr_depth_limit <= max_depth_limit)
		{
			//if a good link combination is found, this top level loop
			//determines the link to build
			for (int i = 1; i <= num_connections; i++)
			{
				//as soon as a link is found, stop
				if (computer_move.isActionSelected()) return;
				
				boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
				if (can_build_link)
				{
					//temporarily build the link (by simply setting the player_id of the link to computer_player_id)
					brass_game.buildTestLink(i, computer_player_id);
					boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
					if (can_player_sell_cotton)
					{
						computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), i);
	System.out.println("Sell cotton link top level");					
					}
					else if (curr_depth < curr_depth_limit)
					{
						getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, curr_depth_limit, curr_depth + 1, num_connections, i);
					}
					//remove the temporary link (by simply setting the player_id of the link to 0)
					brass_game.removeTestLink(i);
				}
			}
			curr_depth_limit++;
		}
	}
	
	private void getAllInfoForLinkToSellCottonActionRec(BrassComputerPlayerAction computer_move, int computer_player_id, int max_depth, int curr_depth, int num_connections, int first_connection)
	{
		for (int i = 1; i <= num_connections; i++)
		{
			//as soon as a link is found, stop
			if (computer_move.isActionSelected()) return;
			
			boolean can_build_link = brass_game.canBuildLink(i, computer_player_id);
			if (can_build_link)
			{
				//temporarily build the link (by simply setting the player_id of the link to computer_player_id)
				brass_game.buildTestLink(i, computer_player_id);
				boolean can_player_sell_cotton = brass_game.canSellCotton(computer_player_id);
				if (can_player_sell_cotton)
				{
					computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), first_connection);
	System.out.println("Sell cotton link depth = " + curr_depth);					
				}
				else if (curr_depth < max_depth)
				{
					getAllInfoForLinkToSellCottonActionRec(computer_move, computer_player_id, max_depth, curr_depth + 1, num_connections, first_connection);
				}
				//remove the temporary link (by simply setting the player_id of the link to 0)
				brass_game.removeTestLink(i);
			}
		}
	}
	
	private void getAllInfoForSortedLinkAction(BrassComputerPlayerAction computer_move, int computer_player_id)
	{	
		List<Integer> sorted_links = brass_game.getSortedLinks();
		int num_connections = sorted_links.size();
		for (int i = 1; i <= num_connections; i++)
		{
			int link_id = sorted_links.get(i-1);
			boolean can_build_link = brass_game.canBuildLink(link_id, computer_player_id);
			if (can_build_link)
			{
				computer_move.selectLinkAction(getCardForNonBuildAction(computer_player_id), link_id);
System.out.println("Sorted links");				
				return;
			}
		}
	}
	
	
}
